package com.swarup.tutorial.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.swarup.tutorial.model.Response;

@RestController
@RequestMapping("/sample")
public class WebController {
	
	/*@Autowired
	KafkaLogService kafkaLogService;*/
	
	private final int MIN = 100;
	private final int MAX = 1000;
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Response> getTest() {
		int amount = MIN + (int) (Math.random() * ((MAX - MIN) + 1));

		Response response = new Response();
		response.setAmount(String.valueOf(amount));
		response.setMerchantName("Demo Merchant");
		response.setTransactionId(UUID.randomUUID().toString().replace("-", ""));
		response.setStatusCode("200");
		response.setRespMessage("Hello");
		
		logger.debug(response.toString());
		
		/*kafkaLogService.loggingToKafka(response.getTransactionId()
										+" "+response.getStatusCode()
										+" "+response.getAmount()
										+" "+response.getMerchantName());*/
		
		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}
}
