package com.swarup.tutorial.controller;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

//@Service
public class KafkaLogService{
	public static final String KAFKA_TRANSACTION_LOG = "KafkaTransactionLog";
	private org.slf4j.Logger _log = LoggerFactory.getLogger(KafkaLogService.class);	
	private static KafkaLogService kafkaTxnLogger = new KafkaLogService(KAFKA_TRANSACTION_LOG);	
	
	public KafkaLogService() {}
	
	private KafkaLogService(String str) {
		_log = LoggerFactory.getLogger(str);
	}
	
	public void loggingToKafka(String contentToLog){
		kafkaTxnLogger._log = LoggerFactory.getLogger(KAFKA_TRANSACTION_LOG);
		kafkaTxnLogger._log.info(contentToLog);
	}
}
