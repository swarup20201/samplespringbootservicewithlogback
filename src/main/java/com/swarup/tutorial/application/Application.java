package com.swarup.tutorial.application;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.swarup.tutorial.controller")
public class Application implements CommandLineRunner{

	public void run(String... arg0) throws Exception {
		
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
